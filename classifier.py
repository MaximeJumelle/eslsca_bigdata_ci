import random
import numpy as np

from sklearn import datasets
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

dataset = datasets.load_breast_cancer()
X = dataset.data  # we only take the first two features.
y = dataset.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2,
                                                    shuffle=True)

print(X[112], y[112])

model = LogisticRegression(C=1e5, solver='lbfgs')
model.fit(X_train, y_train)


# Prédit l'espèce d'un 2D-array (où chaque ligne correspond à un individu)
def predict(x):
    return model.predict(x)


# Calcule le score du modèle
def compute_score():
    score = accuracy_score(y_test, model.predict(X_test))
    print("Accuracy :", score)
    return score


# Calcule le score d'un modèle "naïf" aléatoire
def random_score():
    y_random = np.asarray([random.randint(0, 1) for i in range(len(y_test))])
    score_random = accuracy_score(y_test, y_random)
    print("Accuracy random :", score_random)
    return score_random


print(y)
compute_score()
random_score()
